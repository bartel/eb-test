# EarlyBirds technical test

## Exercise

L'objectif du test est de créer un appel API permettant de retourner des produits de couleur similaire en utilisant l'API Google Vision.

Nous attacherons de l'importance à la structuration du code, au style et à la rigueur.
Le code doit être hébergé sur Github ou Bitbucket, Plus il y a de commit permettant de montrer ton avancement, mieux c'est. Un readme doit être fourni avec les principales instructions permettant d'initialiser le test (description de la stack requise, commandes à exécuter, format des endpoints...).

Pour faire ce test, tu auras besoin :
- d'un catalogue de produits (CSV ci-joint)
- d'un compte sur Google Cloud (gratuit : https://cloud.google.com/vision/)

### 1ère partie : import du catalogue produit

Crées une commande permettant l'import du flux produit au format CSV. Cette commande prend en paramètre l'adresse du fichier CSV.
Les produits sont chargés où tu le souhaites (en RAM, dans un MongoDB...)

### 2ème partie : récupération des couleurs

Crées une commande permettant l'interrogation de l'API Google Vision pour récupérer la couleur dominante.
Attention, le catalogue contient 500 produits hors l'API Google Vision en version gratuite est limitée à 1000 calls. Il est donc fortement recommandé de faire des tests sur un petit nombre de produits pour ne pas être bloqué. Néanmoins, tu auras besoin de valider le fonctionnement sur l'ensemble du catalogue. Quand tu es prêt, récupères les couleurs dominantes pour l'ensemble du catalogue et persiste les.

### 3ème partie : création du endpoint de recommandation

Crées une API avec un endpoint prenant en paramètre un ID de produit et retournant les produits dont la couleur dominante est la plus proche
Tu peux utiliser (ou t'inspirer) de ce module : https://github.com/gausie/colour-proximity
Tout autre méthode est également acceptée.

Voici les points qui seront évalués :

Efficacité :
    Est-ce que le test fonctionne ?
    Facilité d'installation / Explications
    Documentation (readme)

Forme :
    Propreté du code
    Qualité des commentaires
    Qualité des commits (nombre, commentaires...)

Architecture :
    Structuration globale
    Choix techniques (modules, framework,...)
    Utilisation de patterns connus/efficaces

Technique :
    Niveau de connaissance NodeJS
    Niveau de maitrise ES6, Promesses...

Prospective :

    Capacité à progresser / Solidité des bases

Bonus (non demandé dans l'énnoncé) :

    Tests unitaires et qualité

## Solution

### Preface

In this test I didn't use Google Vision API, my only reason is that I'm not ready to provide my credit card number in order to evaluate GCP for free, honestly speaking I'm not ready to provide my personal payment information to the Google corporation at all.

To tackle this issue I've created my own micro-vision API, which is able to detect predominant colour of a given picture(more exactly its URL). Such kind of problems is not a big deal in Data Science so I've used greedy K-Mean clustering algorithm,  that I've exposed through a Dockerized API. In terms of [time complexity](https://stackoverflow.com/questions/18634149/what-is-the-time-complexity-of-k-means) K-Mean is just a disaster(can be classified as a NP-hard problem), but for the testing purposes it's just enough.

### Data quality

During first attempts to explore and visualise data, I've noticed that most parts of exposed pictures contain a lot of colour noise. We can easily demonstrate this whitish noise with a given picture:

![t-shirt](./docs/polo.jpg)

Where colour distribution histogram is:

![histogram](./docs/figs.png) 

As you can see, surrounding whitish colour is dominant, which is kind of wrong. We can tackle this problem with some deep learning approaches, but it's not a part of this topic. In my solution I just sort extracted colours by their dominance and then take a second colour of the list. It's a big kludge, I'm completely aware, but for given data and time window I can't do better. 

### Code quality

As you may notice my code is pretty straightforward in terms of functional programming. At the first glance, you may think that this code is kind of zygohistomorphicly structured prepromorphism, with all those `x`, `y` and `z`. Please don't misjudge me that soon, I struggle for very concise and readable code and I do consider that short param names(as in math notation) of closures are the right way. Please read this [topic](https://blog.ploeh.dk/2015/08/17/when-x-y-and-z-are-great-variable-names/) to better understand my point.

From the same perspective, I do not produce comments in my code(line by line), because I do consider that the code should be self-documenting(it's kind of cliché by now), but the only living documentation can be the tests we are making. I do like this [topic](https://blog.codinghorror.com/coding-without-comments/) explaining common pitfalls of over-commenting.

Due to lack of time a better configuration and many unit/integration tests were skipped.

### Architectural choices

You may notice that I've used RxJs for data processing. I do believe that in my case it's the best choice available. On the one hand, I was looking to make non-blocking, asynchronous data processing, agnostic to specific sources, destinations or error processing and in the other hand I was compelled to work with 3rd-party component such as the Vision API, so the smoothness management of [back pressure](https://en.wikipedia.org/wiki/Back_pressure) was a crucial factor for me.

Due to lack of time no DB was chosen, everything is stored in raw json file. For production purposes this choice must be revised.

### Prerequisite

Prior to any action below, you have to install followed packages:

Os-wide:
 - Docker >= 18.09.5
 - Nodejs >= 10.15

Npm global:
 - typescript >= 2.3
 - mocha
 - ts-lint
 - ts-node

### How to run

First of all you have to build and run vision image:

```bash
cd python
docker-compose up --build 
```

It can take a while, depending on what type of hardware you are using.

You can test the API by doing followed request:
```http
GET http://localhost:5000/?url=http%3A%2F%2Fimage1.lacoste.com%2Fdw%2Fimage%2Fv2%2FAAQM_PRD%2Fon%2Fdemandware.static%2FSites-FR-Site%2FSites-master%2Fdefault%2FL1212_KC8_24.jpg%3Fsw%3D458%26sh%3D443
```

After image is built out, you can do:

```bash
cd ../node
npm install
```

In order to check the functionality you can run tests
```bash
npm test
```

For data ingestion and local db generation you can run
```bash
npm run ingest-all
### or
ts-node ./src/index.ts --in <PATH_TO_CSV> --out ./sample/db.json
```

You have to store all newly generated data bases under `./sample/db.json`

In order to run restful service you can do:
```bash
npm service
```

You can test the api by doing:
```http
GET http://localhost:8889/color-proximity/L1212-00-031
```
