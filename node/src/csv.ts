import {of} from "rxjs";
import {map, switchMap} from "rxjs/operators";
import {IRawItem} from "./types";
import R from "ramda";

const csv = require('csvtojson');

/**
 * Reads csv data from a file and transform it into json structure
 * @param path path to the csv file
 * @param delimiter csv file delimiter
 */
export function readCsv(path: string, delimiter = ";"): Promise<IRawItem[]> {
  return csv({delimiter})
    .fromFile(path)
    .then((x: IRawItem[]) => x)
}

/**
 * Creates an observable chain (pipeline) for reading, transforming and extracting data from inbound csv file
 * @param reader csv file reader
 * @param path path to csv file
 */
export const getFromCsvPipe = R.curry((reader, path: string) =>
  of(path)
    .pipe(map(x => readCsv(x)))
    .pipe(switchMap(x => x))
);
