import Axios from "axios";
import {Observable} from "rxjs";
import {IRawItem} from "./types";
import {GetColors, getColors, getFromVisionPipe} from "./vision";
import {getFromCsvPipe, readCsv} from "./csv";
import R = require("ramda");
import util from "util";
import fs from "fs";

const argv = require('minimist')(process.argv.slice(2));
const writeFileAsync = util.promisify(fs.writeFile);
const colorGetter: GetColors = getColors(Axios.create());
const writeFile = R.curry(function (path: string, data: string) {
  return writeFileAsync(path, data);
});

/**
 * Main entry function, which composes csv reading and vision communication pipes
 * @param csvPath path to initial csv file
 * @param outputPath path to store generated db
 */
export function getItemsAndGenerateDb(csvPath: string, outputPath: string) {
  const fileWriter = writeFile(outputPath);
  const runChain = R.compose(
    (x: Observable<IRawItem[]>) => getFromVisionPipe(colorGetter, fileWriter, x),
    getFromCsvPipe(readCsv)
  );
  return runChain(csvPath).toPromise();
}

if (argv["in"] && argv["out"]) {
  getItemsAndGenerateDb(argv["in"], argv["out"])
    .then(x => console.log("Done"));
}
