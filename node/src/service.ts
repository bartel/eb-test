import {Request, Response, Next, createServer} from "restify";
import {IColorfulItem} from "./types";
const colorProximity = require("colour-proximity");

type ItemsMap = {[x:string]: string}
const notFound = 404;
const topColorsThreshold = 3; // TODO add it to the configuration
const httpPort = 8889;  // TODO add it to the configuration
const dbPath = "../sample/db.json"; // TODO should be parametrized, no hardcoded values for such kind of config data
const items = require(dbPath) as IColorfulItem[];

/**
 * A hacky function which stands for taking median color because most part of images are "infected" with whitish colors
 * @param x colorful item to operate on
 */
const takeMedianColor = (x: IColorfulItem) =>
  x.colors.sort((a, b) => a.percentage <= b.percentage ? 1 : -1)[1].color;

const itemsMap: ItemsMap = items
  .reduce((acc: ItemsMap, x: IColorfulItem) => ({...acc, [x.id]: takeMedianColor(x)}), {});

const itemsKeys = new Set(Object.keys(itemsMap));

/**
 * Service respond function which takes an id and returns the most close items in terms of color proximity
 * @param request Http request
 * @param response Http response
 * @param next nexter
 */
function respond(request: Request, response: Response, next: Next) {
  const id = request.params.id;
  if (!itemsKeys.has(id)) {
    response.send(notFound, `Nothing found for ${id}`);
    return next();
  }
  const color = itemsMap[id];
  const result = Object.keys(itemsMap)
    .filter(x => x !== id)
    .reduce((acc: any[], x: string) => [...acc, {id: x, proximity: colorProximity.proximity(color, itemsMap[x])}], [])
    .sort((x, y) => x.proximity <= y.proximity ? 1 : -1)
    .slice(0, topColorsThreshold) // TODO Implement more intelligent policy of selection
    .map(x => ({...x, color: itemsMap[x.id]}));
  response.send(result);
  next();
}

const service = createServer();
service.get('/color-proximity/:id', respond);

service.listen(httpPort, function() {
  console.log('%s listening at %s', service.name, service.url);
});
