export interface IColor {
  color: string,
  percentage: number,
}

export interface IColorfulItem {
  id: string;
  photo: string;
  colors: IColor[];
}

export interface IRawItem {
  id: string;
  composition: string;
  gender_id: string;
  photo: string;
  sleeve: string;
  title: string;
  url: string;
}
