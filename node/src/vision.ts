import {AxiosInstance} from "axios";
import {Observable} from "rxjs";
import {IColor, IColorfulItem, IRawItem} from "./types";
import {filter, flatMap, map, reduce} from "rxjs/operators";
import {logErrorAndDie} from "./utils";
import R = require("ramda");

const visionUrl = "http://localhost:5000/?url="; // TODO this url should be a part of configuration


export type GetColors = (x: IRawItem) => Promise<IColorfulItem|null>
export type FileWriter = (data: string) => Promise<any>

/**
 * API caller function, which takes csv raw row uses its data to send HTTP request to the vision API
 * @param axios HTTP client
 * @param item raw csv row
 */
export const getColors = R.curry((axios: AxiosInstance,
                                   item: IRawItem) : Promise<IColorfulItem|null> =>
  axios
    .get(`${visionUrl}${encodeURI(item.photo)}`)
    .then(x => x.data as IColor[])
    .then(x => ({
      colors: x,
      title: item.title,
      id: item.id,
      photo: item.photo
    }))
    .catch(logErrorAndDie)
);

/**
 * Append vision communication pipe to the given source. Speaks to the vision API and saves results to a file.
 *
 * @param colorsGetter colors getter function, API caller
 * @param fileWriter file writer function which flushes data to the file
 * @param source source pipe
 */
export const getFromVisionPipe = R.curry((colorsGetter: GetColors,
                                          fileWriter: FileWriter,
                                          source: Observable<IRawItem[]>) =>
  source
    .pipe(flatMap(x => x))
    .pipe(map(x => ({...x, photo: `http:${x.photo}`})))
    .pipe(map(x => colorsGetter(x)))
    .pipe(flatMap(x => x))
    .pipe(filter(x => x != null))
    .pipe(reduce((acc: IColorfulItem[], x: IColorfulItem) => [...acc, x], []))
    .pipe(filter(x => x.length > 0))
    .pipe(map(x => JSON.stringify(x)))
    .pipe(flatMap(x => fileWriter(x)))
);
