
import {expect} from "chai";
import {random} from "faker";
import {getItemsAndGenerateDb} from "../src";
import {IColorfulItem} from "../src/types";

describe("Index integration specs", () => {

  it("should return analysed images when file path is received", async () => {
    // Arrange
    const csvPath = "./test/assets/small.csv";
    const outPath = `/tmp/${random.alphaNumeric(20)}.json`;
    const sut = getItemsAndGenerateDb;
    // Act
    const actual = await sut(csvPath, outPath);
    // Assert
    expect(require(outPath)).to.not.eql(undefined);
    expect(require(outPath).length).to.eql(3);
    expect(require(outPath).filter((x : IColorfulItem) => x.colors.length !== 3)).to.eql([]);
  });
});
