
import {expect} from "chai";
import sinon from "sinon";
import {random} from "faker";
import {getColors, getFromVisionPipe} from "../src/vision";
import {of} from "rxjs";

describe("Vision module specs", () => {
  it("should write to file all received results", async () => {
    // Arrange
    const result = random.alphaNumeric(1024);
    const item = {id: random.number(200), photo: random.alphaNumeric(100)};
    const colorful = {...item, colors: []};
    const colorsGetter = sinon.stub();
    const fileWriter = sinon.stub();
    const sut = getFromVisionPipe;

    colorsGetter
      .withArgs({...item, photo: `http:${item.photo}`})
      .returns(Promise.resolve(colorful));

    fileWriter
      .withArgs(JSON.stringify([colorful]))
      .returns(Promise.resolve(result));

    // Act
    const actual = await (sut(colorsGetter, fileWriter, of([item as any])).toPromise());

    // Assert
    expect(result).to.eql(actual);
  });

  it("should get colors when api gives it", async () => {
    // Arrange
    const colors = [random.alphaNumeric(6), random.alphaNumeric(6)];
    const axios = {get: sinon.stub()};
    const item = {id: random.number(200), photo: random.alphaNumeric(100)};
    const sut = getColors;

    axios.get
      .withArgs(sinon.match(x => x.endsWith(item.photo)))
      .returns(Promise.resolve({data: colors}));

    // Act
    const actual = await sut(axios as any, item as any);

    // Assert
    expect(colors).to.eql((actual || {colors: []}).colors);
    expect(item.photo).to.eql((actual || {photo: []}).photo);
    expect(item.id).to.eql((actual || {id: []}).id);
  });

  it("should get colors when api call crashes", async () => {
    // Arrange
    const colors = [random.alphaNumeric(6), random.alphaNumeric(6)];
    const axios = {get: sinon.stub()};
    const item = {id: random.number(200), photo: random.alphaNumeric(100)};
    const sut = getColors;

    axios.get
      .withArgs(sinon.match(x => x.endsWith(item.photo)))
      .returns(Promise.reject("[Exception] Nasty one"));

    // Act
    const actual = await sut(axios as any, item as any);

    // Assert
    expect(null).to.eql(actual);
  });

  it("should write nothing when colors getter returns nulls", async () => {
    // Arrange
    const result = undefined;
    const item = {id: random.number(200), photo: random.alphaNumeric(100)};
    const colorful = {...item, colors: []};
    const colorsGetter = sinon.stub();
    const fileWriter = sinon.stub();
    const sut = getFromVisionPipe;

    colorsGetter
      .withArgs({...item, photo: `http:${item.photo}`})
      .returns(Promise.resolve(null));

    fileWriter
      .throws("Not supposed to happen");

    // Act
    const actual = await (sut(colorsGetter, fileWriter, of([item as any])).toPromise());

    // Assert
    expect(result).to.eql(actual);
  });
});
