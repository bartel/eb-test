from flask import Flask, request, json
import cv2
import numpy as np
from sklearn.cluster import KMeans
import imutils

app = Flask(__name__)


def find_histogram(cluster):
    """
  finds a histogram with k-clusters
  :param: cluster
  :return: histogram
  """
    numLabels = np.arange(0, len(np.unique(cluster.labels_)) + 1)
    (hist, _) = np.histogram(cluster.labels_, bins=numLabels)

    hist = hist.astype("float")
    hist /= hist.sum()

    return hist


def hexify_colors(histogram, centroids):
    """
  converts centroids to hex colors
  :param histogram: generated histogram
  :param centroids: received centroids
  :return: percentage, hex color map
  """
    for (percentage, color) in zip(histogram, centroids):
        hex = '#%02x%02x%02x' % tuple(color.astype("uint8"))
        yield {"color": hex, "percentage": percentage}


@app.route('/')
def analyse_colors():
    url = request.args.get('url')
    original_img = imutils.url_to_image(url)
    rgb_img = cv2.cvtColor(original_img, cv2.COLOR_BGR2RGB)
    reshaped_img = rgb_img.reshape((rgb_img.shape[0] * rgb_img.shape[1], 3))
    clusters = KMeans(n_clusters=3)
    clusters.fit(reshaped_img)

    histogram = find_histogram(clusters)
    colors = list(hexify_colors(histogram, clusters.cluster_centers_))
    return json.dumps(colors)


if __name__ == '__main__':
    app.run(debug=True, host='0.0.0.0')
